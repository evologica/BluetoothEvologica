package com.example.brunoagrizzi.evologicabt;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import com.example.brunoagrizzi.evologicabt.DataStorage;

/**
 * Created by brunoagrizzi on 05/04/2016.
 */
public class DataListener extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 3;

    public static final String MY_PREFS_NAME = "MyUUID";

    private static final String TAG = "DataListener";

    private StringBuffer mOutStringBuffer;

    private ListView mListView;

    private String mConnectedDeviceName = null;

    private BluetoothAdapter mBluetoothAdapter = null;
    private Set<BluetoothDevice> pairedDevices;

    private BluetoothThreadManager mService = null;

    private ArrayAdapter<String> mArrayAdapter;

    ArrayList<HashMap<String,String>> mMensagens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados);

        mListView = (ListView)findViewById(R.id.list);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        initialService();
        findDevice();
    }


    public void findDevice(){
        pairedDevices = mBluetoothAdapter.getBondedDevices();
        Iterator<BluetoothDevice> it = pairedDevices.iterator();
        while(it.hasNext()) {
            BluetoothDevice bt = it.next();
            if (bt.getName().equalsIgnoreCase(getIntent().getStringExtra("dispositivo"))) {
                String address = bt.getAddress();
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                mService.connect(device, true);
                return;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else if (mService == null) {
            initialService();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            mService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mService != null) {
            if (mService.getState() == BluetoothThreadManager.STATE_NONE) {
                mService.start();
            }
        }
    }

    private void initialService() {

        mMensagens = new ArrayList<HashMap<String,String>>();


        //mArrayAdapter = new ArrayAdapter<String>(DataListener.this, R.layout.message);
        //mArrayAdapter = new ArrayAdapter<String>(DataListener.this,android.R.layout.simple_list_item_1);
        //mListView.setAdapter(mArrayAdapter);

        mService = new BluetoothThreadManager(DataListener.this, mHandler,UUID.fromString(getIntent().getStringExtra("UUID")));
        saveUUID(getIntent().getStringExtra("UUID"));
        mOutStringBuffer = new StringBuffer("");
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothThreadManager.STATE_CONNECTED:
                            //mArrayAdapter.clear();
                            mMensagens.clear();
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;

                    String writeMessage = new String(writeBuf);

                    mArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;

                    HashMap<String, String> mMsg = new HashMap<String, String>();
                    //String readMessage = new String(readBuf, 0, msg.arg1);
                    //mArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);

                    String[] from = new String[]{"content", "timestamp"};
                    int[] to = new int[]{R.id.text1,R.id.text2};

                    mMsg.put("content",mConnectedDeviceName + ":  " + toHexString(readBuf,msg.arg1));
                    DateFormat timestamp = new SimpleDateFormat("HH:mm:ss.SSS");
                    Date hora = Calendar.getInstance().getTime();
                    mMsg.put("timestamp",timestamp.format(hora));
                    mMensagens.add(mMsg);

                    SimpleAdapter simpleAdapter = new SimpleAdapter(DataListener.this,mMensagens,R.layout.message,from,to);
                    mListView.setAdapter(simpleAdapter);
                    //mArrayAdapter.add(mConnectedDeviceName + ":  " + toHexString(readBuf,msg.arg1));
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    if (null != DataListener.this) {
                        Toast.makeText(DataListener.this, "Connected to "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    if (null != DataListener.this) {
                        Toast.makeText(DataListener.this, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };
    public String toHexString(byte[] bytes, int num)
    {
        final char[] HEX_CHARS = {
                '0', '1', '2' ,'3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F'
        };
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < bytes.length && i < num; i++) {
            byte b = bytes[i];
            sb.append(new char[] {HEX_CHARS[(b >> 4) & 0x0f], HEX_CHARS[b & 0x0f]});
            sb.append(" ");
        }

        return sb.toString();
    }
    public void saveUUID(String value){
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("UUID", value);
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            //Exporta dados
            DataStorage msgStore = new DataStorage();
            String responseData = "";
            for (HashMap msg: mMensagens) {
                String[] content = msg.get("content").toString().split(":");
                responseData = responseData + msg.get("timestamp").toString() + "\t";
                responseData = responseData + content[1] + "\n";
            }
            String[] initial = mMensagens.get(0).get("timestamp").toString().split(":");
            String dataInitial = initial[0]+"_"+initial[1]+"_"+initial[2];

            String[] finaly = mMensagens.get(mMensagens.size()-1).get("timestamp").toString().split(":");
            String dataFinaly = finaly[0]+"_"+finaly[1]+"_"+finaly[2];

            msgStore.salvarExternalStorage(responseData,mConnectedDeviceName,dataInitial + "-" + dataFinaly);
            Toast.makeText(DataListener.this, "Storage success" , Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
