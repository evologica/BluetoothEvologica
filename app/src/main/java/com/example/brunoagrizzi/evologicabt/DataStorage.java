package com.example.brunoagrizzi.evologicabt;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by brunoagrizzi on 28/04/2016.
 */
public class DataStorage {

    boolean mExternalStorageAvailable = false;
    boolean mExternalStorageWriteable = false;


    public void salvarExternalStorage(String text,String nameDir,String nameArq) {

        // Obtém o estado do storage.
        String mediaState = Environment.getExternalStorageState();

        // Testa se ele está disponível.
        if (mediaState.equals(Environment.MEDIA_MOUNTED)) {

            FileWriter fileWriter = null;
            try {
                // Cria o arquivo onde serão salvas as informações.
                File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS + File.separator + nameDir);
                if (!dir.exists())
                    dir.mkdirs();

                File file = new File(dir, nameArq+".txt");
                fileWriter = new FileWriter(file, true);

                fileWriter.append(text);
                //fileWriter.append("/n");// Quebra de linha.
                //fileWriter.append(String.valueOf(numero));

                // Escreve no arquivo.
                fileWriter.flush();

            } catch (IOException e) {
                Log.e("Erros", "Erro ao salvar usando External Storage", e);
            } finally {
                // Fecha os recursos.
                if (fileWriter != null) {
                    try {fileWriter.close();}
                    catch(Exception e){}
                }
            }
        }
    }



    public void checkStorage() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Podemos ler e escrever os meios de comunicação
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Só podemos ler a mídia
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Outra coisa que está errada.  Pode ser um de muitos outros estados, mas tudo o que precisamos
            // Para saber é que não sabem ler nem escrever
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
    }
}
